function checkIfStringHasOnlyDigits(_string) {
    for (let i = _string.length - 1; i >= 0; i--) {
        const codeValue = _string.charCodeAt(i);
        if (codeValue < 48 || codeValue > 57)
            return false
    }
    return true
}

const getSum = (str1, str2) => {
  // add your implementation below
    if (typeof str1 != 'string' ||
        typeof str2 != 'string') {
        return false;
    }

    if (!checkIfStringHasOnlyDigits(str1) || !checkIfStringHasOnlyDigits(str2)) {
        return false;
    }

    if (str1.length > str2.length) {
        let t = str1;
        str1 = str2;
        str2 = t;
    }

    let str = "";

    let n1 = str1.length, n2 = str2.length;

    str1 = str1.split("").reverse().join("");
    str2 = str2.split("").reverse().join("");

    let carry = 0;
    for (let i = 0; i < n1; i++) {

        let sum = ((str1[i].charCodeAt(0) -
            '0'.charCodeAt(0)) +
            (str2[i].charCodeAt(0) -
                '0'.charCodeAt(0)) + carry);
        str += String.fromCharCode(sum % 10 +
            '0'.charCodeAt(0));

        carry = Math.floor(sum / 10);
    }

    for (let i = n1; i < n2; i++) {
        let sum = ((str2[i].charCodeAt(0) -
            '0'.charCodeAt(0)) + carry);
        str += String.fromCharCode(sum % 10 +
            '0'.charCodeAt(0));
        carry = Math.floor(sum / 10);
    }

    if (carry > 0)
        str += String.fromCharCode(carry +
            '0'.charCodeAt(0));

    str = str.split("").reverse().join("");

    return str;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
    let numPosts = 0;
    let numComments = 0;

    listOfPosts.map(p => {
        if (p.author == authorName) {
            numPosts++;
        }

        if (p.comments) {
            p.comments.map(c => {
                if (c.author == authorName) {
                    numComments++;
                }
            })
        }
    })

    return `Post:${numPosts},comments:${numComments}`;
};

const tickets=(people)=> {
  // add your implementation below
    let twentyFive = 0;
    let fifty = 0;

    for (let i = 0; i < people.length; i++) {

        if (people[i] == 25) {
            twentyFive += 1;
        }

        if (people[i] == 50) {
            twentyFive -= 1;
            fifty += 1;
        }

        if (people[i] == 100) {
            if (fifty == 0 && twentyFive >= 3) {
                twentyFive -= 3;
            } else {
                twentyFive -= 1;
                fifty -= 1;
            }
        }

        if (twentyFive < 0 || fifty < 0) {
            return 'NO';
        }
    }

    return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
